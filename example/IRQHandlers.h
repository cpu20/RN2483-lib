/**
 * Copyright (c) 2018 Tijl Schepens, Cynthia Blancke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    inc/IRQHandlers.h
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    04-04-2018
  * @brief   IRQHandlers header file.
  *
  *          This file contains all the function
  *           prototypes for the interrupt handlers.
  ******************************************************************************
  */

#ifndef __IRQHANDLERS_H
#define __IRQHANDLERS_H

/** @addtogroup IRQHandlers
 * @{
 */

#include "main.h"
#include "rn2483.h"

/* Variables that need to be found somewhere else. */
extern char LoRaRxBuffer[LORA_RX_BUFFER_LENGTH];
extern uint8_t LoRaRxBufferIndex;

/* Prototypes for the interrupt handlers. */
void LEUART0_IRQHandler(void);

/** @}*/

#endif /* __IRQHANDLERS_H */
