/**
 * Copyright (c) 2018 Tijl Schepens, Cynthia Blancke
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

/**
  ******************************************************************************
  * @file    src/main.c
  * @author  Tijl Schepens
  * @version V1.0.0
  * @date    04-04-2018
  * @brief   Main file.
  *
  *          Where it all comes togheter.
  ******************************************************************************
  */

/** @addtogroup Example
 * @{
 */

/* The LoRa module driver. */
#include "rn2483.h"

volatile uint32_t msTicks; /* counts 1ms timeTicks */

/***************************************************************************//**
 * @brief SysTick_Handler
 * Interrupt Service Routine for system tick counter
 ******************************************************************************/
void SysTick_Handler(void)
{
  msTicks++;       /* Increment counter. */
}

int main(void)
 {
  /* Chip errata */
  CHIP_Init();

  /* Setup SysTick Timer for 1 msec interrupts  */
  if (SysTick_Config(CMU_ClockFreqGet(cmuClock_CORE) / 1000)) {
    while (1);
  }

  /* Initialize the LoRa module. */
  rn2483_init(setupABP);
  /* Peform an Activation By Personalization. Retry 4 times. */
  bool result = false;
  uint8_t tries = 0;
  while(result == false && tries < 4){
    result = rn2483_join_abp();
    tries++;
  }

  if(result == false){
    while(1);
  }

  /* Infinite main loop */
  while (1) {
    char payload[16] = { 0 };

		/* Send some data. */
    sprintf(payload, "%X%X%X%X", 0xF, 0xC, 0x2, 0x5);
    rn2483_send_payload(1, payload);
  }
}

/** @}*/
