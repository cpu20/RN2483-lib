/* Licensing needs to be discussed. */

/**
  ******************************************************************************
  * @file    inc/main.h
  * @author  Jaimy Declercq, Tijl Schepens
  * @version V1.2.0
  * @date    06-03-2018
  * @brief   RN2483 library source file.
  *
  *          All the good stuff happens here. All the functions
  *          are implemented here. Variables can be found here,
  *          like device keys, UART buffer,...
  ******************************************************************************
  */

/** @addtogroup RN2483
 * @{
 */

#include "rn2483.h"

/* Variables and stuff */
/* Buffers */
char LoRaRxBuffer[LORA_RX_BUFFER_LENGTH];
char LoRaResponse[LORA_RX_BUFFER_LENGTH] = { 0 };
uint8_t loraLoRaResponseIndex = 0;
/* Index to keep track of the current position in the LoRa
 * receive buffer. */
uint8_t LoRaRxBufferIndex = 0;

/* Keys for OTAA */
const char *hweui  = "";
const char *appEui = "";
const char *appKey = "";

/* Keys for ABP */
const char *devAddr = "";
const char *nwkSKey = "";
const char *appSKey = "";

/* Used for timing. */
extern uint32_t msTicks;

/* Commands */
const char macSetDeveui[]   = "mac set deveui ";
const char macSetAppeui[]   = "mac set appeui ";
const char macSetAppkey[]   = "mac set appkey ";
const char macSave[]        = "mac save";
const char enableAdr[]      = "mac set adr on";
const char joinOtaa[]       = "mac join otaa";
const char setupRetx[]      = "mac set retx ";
const char setSf[]          = "mac set sf sf";
const char powerIdx[]       = "mac set pwridx 1";
const char outputPowerMax[] = "radio set pwr 15"; // 15 dBm ter compensatie van verliezen in antenne trace
const char sendPayload[]    = "mac tx cnf ";

const char macGetDevaddr[]  = "mac get devaddr";
const char macGetDeveui[]   = "mac get deveui";
const char macGetAppeui[]   = "mac get appeui";
const char macGetrxdelay1[] = "mac get rxdelay1";
const char macGetrxdelay2[] = "mac get rxdelay2";

char devaddr[50]  = { 0 };
char deveui[50]   = { 0 };
char appeui[50]   = { 0 };
char appskey[50]  = { 0 };
char nwkskey[50]  = { 0 };
char rxdelay1[50] = { 0 };
char rxdelay2[50] = { 0 };

/* Possible answers to mac request*/
const char okAnswer[]               = "ok";
const char invalidParameterAnswer[] = "invalid_param";
const char notJoinedAnswer[]        = "not_joined";
const char noFreeChAnswer[]         = "no_free_ch";
const char silentAnswer[]           = "silent";
const char frameCounterErrAnswer[]  = "frame_counter_err_rejoin_needed";
const char invalidDataLenAnswer[]   = "invalid_data_len";
const char busyAnswer[]             = "busy";
const char acceptedAnswer[]         = "accepted";
const char deniedAnswer[]           = "denied";
const char macPauseAnswer[]         = "4294967245";
const char getVerAnswer[]           = "RN2483";

/* Possible answers to send/join request*/
const char macRxAC[] = "mac_rx 1 AC";
const char macRxAF[] = "mac_rx 1 AF";
const char macTxOk[] = "mac_tx_ok";
const char macErr[]  = "mac_err";

void rn2483_get_mac()
{
  send_command(macGetDevaddr);
  strcpy(devaddr, LoRaResponse);

  send_command(macGetDeveui);
  strcpy(deveui, LoRaResponse);

  send_command(macGetAppeui);
  strcpy(appeui, LoRaResponse);

  send_command("mac get nwskey");
  strcpy(nwkskey, LoRaResponse);

  send_command(macGetrxdelay1);
  strcpy(rxdelay1, LoRaResponse);

  send_command(macGetrxdelay2);
  strcpy(rxdelay2, LoRaResponse);

}

uint8_t setup_duty_cycle()
{
  ReceiveResponse commandResponse;

  /* Default duty cycle is 0.33% */
  /* Set channel 1 to 1% duty cycle*/
  commandResponse = send_command("mac set ch dcycle 0 9");

  if (commandResponse != ReceivedOK) {
    return 0;
  }
  else {
    commandResponse = send_command("mac set ch dcycle 0 9");
    if (commandResponse != ReceivedOK)
      return 0;
  }

  /* Set channel 2 to 1% duty cycle*/
  //success = false;
  commandResponse = send_command("mac set ch dcycle 1 9");
  if (commandResponse != ReceivedOK) {
    return 0;
  }

  /* Set channel 3 to 1% duty cycle*/
  //success = false;
  commandResponse = send_command("mac set ch dcycle 2 9");
  if (commandResponse != ReceivedOK) {
    return 0;
  }
  return 1;
}

uint8_t setup_channels()
{
  ReceiveResponse commandResponse;

  /* Setup fourth channel at 867.1 MHz */
  commandResponse = send_command("mac set ch freq 3 867100000");

  if (commandResponse == ReceivedOK) {
    commandResponse = send_command("mac set ch dcycle 3 99");
    if (commandResponse != ReceivedOK)
      return 0;
  }
  /* Setup fifth channel at 867.3 MHz*/
  commandResponse = send_command("mac set ch freq 4 867300000");

  if (commandResponse == ReceivedOK) {
    commandResponse = send_command("mac set ch dcycle 4 0");
    if (commandResponse != ReceivedOK)
      return 0;
  }
  return 1;
}

/**
 * @Set up how many times the module does a retransmission.
 *
 * @param Retransmissions   The amount of retransmissions.
 *
 * @retval RN2483_OK:     Success.
 * @retval RN2483_Error:  Something went wrong.
 *
 * @see RN2483_Status
 */
RN2483_Status setup_retransmissions(int Retransmissions)
{
  ReceiveResponse commandResponse;
  char intToChar[2];
  sprintf(intToChar, "%d", Retransmissions);
  char command[100];
  strcpy(command, setupRetx);
  strcat(command, intToChar);

  commandResponse = send_command(command);
  if (commandResponse != ReceivedOK)
    return RN2483_OK;
  return RN2483_Error;
}

/**
 * @brief Send a command to the RN2483.
 *
 * @param command   A pointer to the command that has to be sent.
 *
 * @retval ReceiveTimeout:              A timeout occurred while waiting for a response.
 * @retval ReceiveOk:                   Receiving a response from the LoRa module was successful.
 * @retval ReceiveError:                An error occurred while receiving the response.
 * @retval ReceivedAccepted:            The join procedure was successful.
 * @retval ReceivedBusy:                The MAC is not in idle state.
 * @retval ReceivedDenied:              The module attempted to join the network but was rejected.
 * @retval ReceivedFrameCounterError:   The frame counter rolled over.
 * @retval ReceivedInvalidDataLength:   Payload length is greater than the max application payload
                                        length corresponding to the current data rate.
 * @retval ReceivedInvalidParameter:    Invalid parameters were send to the module.
 * @retval ReceivedMACRxAC:             Transmission was successful on port 1 and got response AC.
 * @retval ReceivedMACRxAF:             Transmission was successful on port 1 and got response AF.
 * @retval ReceivedMACTxOK:             Uplink to the server was successful with no downlink response.
 * @retval ReceivedMACError:            Transmission was unsuccessful. No ACK was received from the server.
 * @retval ReceivedNotJoined:           The module hasn't joined the network jet!
 * @retval ReceivedNoFreeCh:            All channels are currently busy.
 * @retval ReceivedOK:                  Received command and parameters are valid.
 * @retval ReceivedSilent:              The module is in a silent immediately state.
 * @retval ReceivedMACPause:            MAC is paused and wasn't resumed again.
 * @retval ReceivedVersion:             Got the module firmware version.
 * @retval ReceivedNULL:                Response was NULL.
 *
 * @see ReceiveResponse
 */
ReceiveResponse send_command(const char * command)
{
  /* Presume: command is ended with NULL element. */
  int command_index = 0;

  /* Make sure the LoRa command ends with a carriage return and line feed. */
  char cmd[100] = { 0 };
  //char *cmd = (char *)malloc(sizeof(char) * (sizeof(command) + 3));
  strcat(cmd, command);
  strcat(cmd, "\r\n");

  /* Reset the buffer index. */
  LoRaRxBufferIndex = 0;

  /* Keep sending chars to the LoRa modem until NULL element is reached. */
  while (cmd[command_index] != '\0')
    LEUART_Tx(LEUART0, cmd[command_index++]);

  return ParseResponse(LORA_TIMEOUT);
}

/**
 * @brief Low level function that parses responses from the RN2483.
 *
 * This function will wait for a response from the RN2483. A timeout
 * is used to make sure this function won't hang. It takes the response
 * form the UART buffer, parses it and returns what was received.
 *
 * @param timeout     A timeout in milliseconds
 *
 * @retval ReceiveTimeout:              A timeout occurred while waiting for a response.
 * @retval ReceiveOk:                   Receiving a response from the LoRa module was successful.
 * @retval ReceiveError:                An error occurred while receiving the response.
 * @retval ReceivedAccepted:            The join procedure was successful.
 * @retval ReceivedBusy:                The MAC is not in idle state.
 * @retval ReceivedDenied:              The module attempted to join the network but was rejected.
 * @retval ReceivedFrameCounterError:   The frame counter rolled over.
 * @retval ReceivedInvalidDataLength:   Payload length is greater than the max application payload
                                        length corresponding to the current data rate.
 * @retval ReceivedInvalidParameter:    Invalid parameters were send to the module.
 * @retval ReceivedMACRxAC:             Transmission was successful on port 1 and got response AC.
 * @retval ReceivedMACRxAF:             Transmission was successful on port 1 and got response AF.
 * @retval ReceivedMACTxOK:             Uplink to the server was successful with no downlink response.
 * @retval ReceivedMACError:            Transmission was unsuccessful. No ACK was received from the server.
 * @retval ReceivedNotJoined:           The module hasn't joined the network jet!
 * @retval ReceivedNoFreeCh:            All channels are currently busy.
 * @retval ReceivedOK:                  Received command and parameters are valid.
 * @retval ReceivedSilent:              The module is in a silent immediately state.
 * @retval ReceivedMACPause:            MAC is paused and wasn't resumed again.
 * @retval ReceivedVersion:             Got the module firmware version.
 * @retval ReceivedNULL:                Response was NULL.
 *
 * @see ReceiveResponse
 */
ReceiveResponse ParseResponse(uint16_t timeout)
{
  uint32_t startTicks = msTicks;
  /* Wait here until NULL element of answer is reached. Let IRQ handle the reading. */
  while (LoRaRxBuffer[LoRaRxBufferIndex - 1] != '\n') {
    /* Make sure we don't get stuck here if something goes wrong. */
    if(msTicks - startTicks > timeout) {
      return ReceiveTimeout;
    }
  }

  /* Reset the buffer index. */
  LoRaRxBufferIndex = 0;

  switch(LoRaRxBuffer[0]){
    case 'a':
      if(StrCompare(LoRaRxBuffer, acceptedAnswer, sizeof(acceptedAnswer) - 1))
        return ReceivedAccepted;
      else
        return ReceiveError;
      break;
    case 'b':
      if(StrCompare(LoRaRxBuffer, busyAnswer, sizeof(busyAnswer) - 1))
        return ReceivedBusy;
      else
        return ReceiveError;
      break;
    case 'd':
      if(StrCompare(LoRaRxBuffer, deniedAnswer, sizeof(deniedAnswer) - 1))
        return ReceivedDenied;
      else
        return ReceiveError;
      break;
    case 'f':
      if(StrCompare(LoRaRxBuffer, frameCounterErrAnswer, sizeof(frameCounterErrAnswer) - 1))
        return ReceivedFrameCounterError;
      else
        return ReceiveError;
      break;
    case 'i':
      if(StrCompare(LoRaRxBuffer, invalidDataLenAnswer, sizeof(invalidDataLenAnswer) - 1))
        return ReceivedInvalidDataLength;
      else if(StrCompare(LoRaRxBuffer, invalidParameterAnswer, sizeof(invalidParameterAnswer) - 1))
        return ReceivedInvalidParameter;
      else
        return ReceiveError;
      break;
    case 'm':
      if(StrCompare(LoRaRxBuffer, macRxAC, sizeof(macRxAC) - 1))
        return ReceivedMACRxAC;
      else if(StrCompare(LoRaRxBuffer, macRxAF, sizeof(macRxAF) - 1))
        return ReceivedMACRxAF;
      else if(StrCompare(LoRaRxBuffer, macTxOk, sizeof(macTxOk) - 1))
        return ReceivedMACTxOK;
      else if(StrCompare(LoRaRxBuffer, macErr, sizeof(macErr) - 1))
        return ReceivedMACError;
      else
        return ReceiveError;
    case 'n':
      if(StrCompare(LoRaRxBuffer, notJoinedAnswer, sizeof(notJoinedAnswer) - 1))
        return ReceivedNotJoined;
      else if(StrCompare(LoRaRxBuffer, noFreeChAnswer, sizeof(noFreeChAnswer) - 1))
        return ReceivedNoFreeCh;
      else
        return ReceiveError;
      break;
    case 'o':
      if(StrCompare(LoRaRxBuffer, okAnswer, sizeof(okAnswer) - 1))
        return ReceivedOK;
      else
        return ReceiveError;
      break;
    case 'R':
      if(StrCompare(LoRaRxBuffer, getVerAnswer, sizeof(getVerAnswer) - 1))
        return ReceivedVersion;
      else
        return ReceiveError;
    case 's':
      if(StrCompare(LoRaRxBuffer, silentAnswer, sizeof(silentAnswer) - 1))
        return ReceivedSilent;
      else
        return ReceiveError;
      break;
    case '4':
      return ReceivedMACPause;
      break;
    case 0:
      return ReceivedNULL;
      break;
    default:
      return ReceiveError;
      break;
  }
}

/**
 * @brief Low level function to configure the LEUART peripheral.
 *
 * @param None
 *
 * @retval None
 */
void LEUARTInit()
{
  /* Initialize the LEUART peripheral with basic settings. */
  LEUART_Init_TypeDef leUartInit = LEUART_INIT_DEFAULT;

  /* Enable LF domain clocks to be able to use the LF peripherals. */
  CMU_ClockEnable(cmuClock_HFLE, true);
  /* Enable and set LFXO for LFBCLK */
  CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);
  CMU_ClockEnable(cmuClock_LFB, true);
  /* Enable peripheral clocks. */
  CMU_ClockEnable(RN2483_LEUART_CLOCK, true);
  CMU_ClockEnable(cmuClock_GPIO, true);

  /* Do not prescale clock */
  CMU_ClockDivSet(cmuClock_LEUART0, cmuClkDiv_1);

  /* Configure the Rx/Tx lines. */
  GPIO_PinModeSet(RN2483_LEUART_Tx_Port, RN2483_LEUART_Tx_Pin, gpioModePushPull, 1);
  GPIO_PinModeSet(RN2483_LEUART_Rx_Port, RN2483_LEUART_Rx_Pin, gpioModeInput, 0);

  /* Enable the Tx/Rx lines and select the correct pin location. */
  RN2483_LEUART->ROUTE = RN2483_LEUART_PINS_LOC |
      LEUART_ROUTE_TXPEN | LEUART_ROUTE_RXPEN;

  /* Initialize and enable the peripheral. */
  LEUART_Init(RN2483_LEUART, &leUartInit);
  /* Enable data valid interrupt. */
  LEUART_IntEnable(RN2483_LEUART, LEUART_IF_RXDATAV);
  /* Rx buffer overflow interrupt. */
  LEUART_IntEnable(RN2483_LEUART, LEUART_IF_RXOF);
  /* Rx buffer underflow interrupt. */
  LEUART_IntEnable(RN2483_LEUART, LEUART_IF_RXUF);
  NVIC_EnableIRQ(LEUART0_IRQn);
}

/**
 * @brief Init function for the RN2483
 * This function does the LEUART initialization and fully
 * configures the RN2483. A join is not yet performed.
 *
 * @param setupKeys   Configure the RN2483 to use ABP or OTAA for activation.
 *
 * @see JoinSetup
 */
void rn2483_init(JoinSetup setupKeys)
{
  /* Initialize the LEUART peripheral. */
  LEUARTInit();

  /* Enable the LoRa module supply. */
  GPIO_PinOutClear(LORA_ENABLE_PORT, LORA_ENABLE_PIN);

  /* Wait for the module to boot. */
  uint32_t startTicks = msTicks;
  while((msTicks - startTicks) < 200);

  /* Perform an autobaud procedure. */
  LEUART_Tx(LEUART0, 0x00);
  LEUART_Tx(LEUART0, 0x55);

  /* Wait for the autobaud procedure to complete. */
  startTicks = msTicks;
  while((msTicks - startTicks) < 100);

  /* Get the firmware version. (retry 4 times in case it fails) */
  for(int i=0;i<4;i++){
    if(rn2483_get_ver())
      break;
  }

  /* Set dutycyle on defualt bands to 100%*/
  setup_duty_cycle();

  /* Setting up 2 extra channels */
  setup_channels();

  /* Setup the essentials for ABP based connection */
  if(setupKeys == setupABP){
    rn2483_set_devaddr();
    rn2483_set_nwskey();
    rn2483_set_appskey();
  }
  /* Setup the essentials for OTAA based activation */
  else{
    rn2483_set_deveui();
    rn2483_set_appeui();
    rn2483_set_appkey();
  }

  rn2483_enable_adr();
  rn2483_save_mac();
  rn2483_set_power_idx();

  /* Setup number of retransmissions in case of unconfirmed uplink */
  setup_retransmissions(3);

}

/**
 * @brief Write the Device EUI to the module.
 *
 * @param None
 *
 * @retval RN2483_OK:     Success.
 * @retval RN2483_Error:  Something went wrong.
 *
 * @see RN2483_Status
 */
RN2483_Status rn2483_set_deveui()
{
  ReceiveResponse commandResponse;
  char command[100];
  strcpy(command, macSetDeveui);
  strcat(command, hweui);

  commandResponse = send_command(command);
  if (commandResponse != ReceivedOK)
    return RN2483_Error;
  return RN2483_OK;
}

uint8_t rn2483_set_appeui()
{
  ReceiveResponse commandResponse;
  char command[100];
  strcpy(command, macSetAppeui);
  strcat(command, appEui);

  commandResponse = send_command(command);
  if (commandResponse != ReceivedOK)
    return 0;
  return 1;
}

uint8_t rn2483_set_appkey()
{
  ReceiveResponse commandResponse;
  char command[100];
  strcpy(command, macSetAppkey);
  strcat(command, appKey);

  commandResponse = send_command(command);
  if (commandResponse != ReceivedOK)
    return 0;
  return 1;
}

uint8_t rn2483_set_power_max()
{
  ReceiveResponse commandResponse;
  /* pause the mac before sending radio command*/
  commandResponse = send_command("mac pause");
  if (commandResponse != ReceivedMACPause)
    return 0;

  commandResponse = send_command(outputPowerMax);
  if (commandResponse != ReceivedOK)
    return 0;

  /* Resuming mac so that us*/
  commandResponse = send_command("mac resume");
  if (commandResponse != ReceivedOK)
    return 0;
  return 1;
}

uint8_t rn2483_mac_resume()
{
  ReceiveResponse commandResponse;
  /* Resuming mac so that us*/
  commandResponse = send_command("mac resume");
  if (commandResponse != ReceivedOK)
    return 0;
  return 1;
}

uint8_t rn2483_enable_adr()
{
  ReceiveResponse commandResponse;
  commandResponse = send_command(enableAdr);
  if (commandResponse != ReceivedOK)
    return 0;
  return 1;
}

uint8_t rn2483_save_mac()
{
  ReceiveResponse commandResponse;
  commandResponse = send_command(macSave);
  if (commandResponse != ReceivedOK)
    return 0;
  return 1;
}

/**
 * @brief Perform an Over The Air Activation.
 *
 * @param None
 *
 * @retval RN2483_JoinOK:     The OTAA succeeded.
 * @retval RN2483_JoinFailed: The OTAA failed.
 *
 * @see RN2483_Status
 */
RN2483_Status rn2483_join_otaa()
{
  ReceiveResponse commandResponse;
  commandResponse = send_command(joinOtaa);
  /* Try sending join request*/
  if (commandResponse == ReceivedOK) {
    /* When request send: wait for ACK */
    commandResponse = ParseResponse(10000);

    /* Hoping for an ACK*/
    if (commandResponse == ReceivedAccepted) {
      return RN2483_JoinOK;
    }
  }

  return RN2483_JoinFailed;
}

bool rn2483_send_payload(uint8_t portNo, char payload[])
{
  ReceiveResponse commandResponse;
  char command[100] = { 0 };
  char portNumber[3] = { 0 };
  strcpy(command, sendPayload);
  sprintf(portNumber, "%d ", portNo);
  strcat(command, portNumber);
  strcat(command, payload);

  commandResponse = send_command(command);

  if (commandResponse == ReceivedOK) {
    /* Tx timeouts can take up to 20s! */
    commandResponse = ParseResponse(21000);
    if(commandResponse != ReceivedMACTxOK){
      return false;
    }
    else
      return true;
  }
  return false;
}

uint8_t rn2483_set_devaddr()
{
  ReceiveResponse commandResponse;
  char command[100] = "mac set devaddr ";
  strcat(command, devAddr);

  commandResponse = send_command(command);

  if (commandResponse != ReceivedOK) {
    return 0;
  }
  return 1;
}

uint8_t rn2483_set_nwskey()
{
  ReceiveResponse commandResponse;
  char command[100] = "mac set nwkskey ";
  strcat(command, nwkSKey);

  commandResponse = send_command(command);

  if (commandResponse != ReceivedOK) {
    return 0;
  }
  return 1;
}

uint8_t rn2483_set_appskey()
{
  ReceiveResponse commandResponse;
  char command[100] = "mac set appskey ";
  strcat(command, appSKey);

  commandResponse = send_command(command);

  if (commandResponse != ReceivedOK) {
    return 0;
  }
  return 1;
}

/**
 * @brief Perform an Activation By Personalization.
 *
 * @param None
 *
 * @retval RN2483_JoinOK:     The ABP succeeded.
 * @retval RN2483_JoinFailed: The ABP failed.
 *
 * @see RN2483_Status
 */
RN2483_Status rn2483_join_abp()
{
  ReceiveResponse commandResponse;

  commandResponse = send_command("mac join abp");

  if (commandResponse == ReceivedOK) {
    /* When request send: wait for ACK */
    commandResponse = ParseResponse(1000);

    /* Hoping for an ACK*/
    if (commandResponse == ReceivedAccepted) {
      return RN2483_JoinOK;
    }
  }

  return RN2483_JoinFailed;
}

uint8_t rn2483_sys_reset()
{
  ReceiveResponse commandResponse;
  char command[100] = "sys reset";

  commandResponse = send_command(command);

  return 1;
}

bool rn2483_send_bytes(uint8_t portNo, char payload[], size_t length)
{
  ReceiveResponse commandResponse;
  uint8_t cycles = 3;
  uint8_t counter = 0;
  bool received = false;
  char command[100] = { 0 };
  char portNumber[3] = { 0 };
  strcpy(command, sendPayload);
  sprintf(portNumber, "%d ", portNo);
  strcat(command, portNumber);
  strcat(command, payload);

  commandResponse = send_command(command);

  if (commandResponse == ReceivedOK) {
    while ((!received) && (counter < cycles)) {
      /* Transmission timeouts take up to 20s! */
      commandResponse = ParseResponse(22000);
      if(commandResponse == ReceivedMACTxOK){
        counter++;
        return true;
      }
    }
  }
  return false;
}

uint8_t rn2483_set_power_idx()
{
  ReceiveResponse commandResponse;

  commandResponse = send_command(powerIdx);

  if (commandResponse != ReceivedOK) {
    return 0;
  }
  return 1;
}

/**
 * @brief Get the version of the module.
 */
uint8_t rn2483_get_ver()
{
  ReceiveResponse commandResponse;
  char command[100] = "sys get ver";

  commandResponse = send_command(command);

  if (commandResponse != ReceivedVersion) {
    return 0;
  }
  return 1;
}

/** @}*/
